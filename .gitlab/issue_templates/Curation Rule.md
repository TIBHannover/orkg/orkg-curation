### Summary

(Summarize the proposed rule concisely)

### What is the object of the rule (What needs curation)?

(Comparison, Contribution, Resource, ...)

### Can the object(s) be viewed online in [orkg](https://www.orkg.org/)?

(If yes, please provide the URL(s))

### What rule do you propose?

(What you want to see implemented on the ORKG curation)

### Arguments _for_ the rule (Pro)

(Reasons that speak for the rule)

### Arguments _against_ the rule (Pro)

(Reasons that might speak against the rule, or limit it's application scope)

### Relevant screenshots

(Please headline each screenshot with "#### What is imporant in this picture")

### Possible rule / implementation

(Try to formulate a pseudocode/written out rule: IF (has_no_description AND has_no_instances) AND_NOT (is_younger_than_a_month OR was_manually_reviewed) DO (...))

/label ~rule
